## Projet Csharp dotNet

##Fonctionnalites prises en compte : 

Crystal report et le Setup.

## L'application permet :

1) Voir tous les livres disponibles dans la librairie et ajouter des livres dans
   la commande d'un client et imprimer le re�u. Item : Liste

2) Ajouter un livre dans une etag�re. Item : Livre
3) Ajouter une etagere, une fois ajout�e elle est directement
  presente au comboBox pour le choix quant il s'agit de l'ajout
  d'un livre. Item : Etag�re
4) Retirer un livre. Item : Retirer
5) Rendre un livre. Item : Rendre
6) Remplacer un livre par un autre. Item : Remplacer

## Installation
 Le setup executable pour installer l'app se trouve dans le dossier DLSetup
## Base de données
 Pour un bon fonctionnement de l'application, pensez à changer le chemin de la base de donnée sur la ligne de connexion dans les tous les fichiers (ligne : SqlConnection connection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\hp\Desktop\BARRY_Abdoulaye_Sadio\biblio\biblio\Biblio.mdf;Integrated Security=True"); partie à changer selon le repertoir où vous avez cloné ce projet : AttachDbFilename=C:\Users\hp\Desktop\BARRY_Abdoulaye_Sadio\biblio\biblio\Biblio.mdf)

## NB :
Si vous changez le chemin de la base de donnée, vous devrez générer un autre setup de l'application.

## Raccourci :
Un raccourci vous est fourni pour voir ce que fait l'pplication 


