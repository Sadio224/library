﻿namespace biblio
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.metroTileAjoutLivre = new MetroFramework.Controls.MetroTile();
            this.metroTileAjoutEtagere = new MetroFramework.Controls.MetroTile();
            this.metroTileRemettreLivre = new MetroFramework.Controls.MetroTile();
            this.metroTileRetirerLivre = new MetroFramework.Controls.MetroTile();
            this.metroTileRemplacerLirve = new MetroFramework.Controls.MetroTile();
            this.metroTileListeLivres = new MetroFramework.Controls.MetroTile();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.metroTile2 = new MetroFramework.Controls.MetroTile();
            this.metroPanel2 = new MetroFramework.Controls.MetroPanel();
            this.metroTile1 = new MetroFramework.Controls.MetroTile();
            this.remplacer1 = new biblio.Remplacer();
            this.rendre1 = new biblio.Rendre();
            this.retirerLivre1 = new biblio.RetirerLivre();
            this.listeLivres1 = new biblio.ListeLivres();
            this.test11 = new biblio.AjoutEtagere();
            this.ajoutLivre1 = new biblio.AjoutLivre();
            this.metroPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroTileAjoutLivre
            // 
            this.metroTileAjoutLivre.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.metroTileAjoutLivre.CustomBackground = true;
            this.metroTileAjoutLivre.Location = new System.Drawing.Point(0, 100);
            this.metroTileAjoutLivre.Name = "metroTileAjoutLivre";
            this.metroTileAjoutLivre.Size = new System.Drawing.Size(196, 55);
            this.metroTileAjoutLivre.TabIndex = 0;
            this.metroTileAjoutLivre.Text = "Livre";
            this.metroTileAjoutLivre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroTileAjoutLivre.TileImage = ((System.Drawing.Image)(resources.GetObject("metroTileAjoutLivre.TileImage")));
            this.metroTileAjoutLivre.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.metroTileAjoutLivre.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall;
            this.metroTileAjoutLivre.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Bold;
            this.metroTileAjoutLivre.UseTileImage = true;
            this.metroTileAjoutLivre.Click += new System.EventHandler(this.metroTileAjoutLivre_Click);
            // 
            // metroTileAjoutEtagere
            // 
            this.metroTileAjoutEtagere.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.metroTileAjoutEtagere.CustomBackground = true;
            this.metroTileAjoutEtagere.Location = new System.Drawing.Point(3, 176);
            this.metroTileAjoutEtagere.Name = "metroTileAjoutEtagere";
            this.metroTileAjoutEtagere.Size = new System.Drawing.Size(196, 51);
            this.metroTileAjoutEtagere.TabIndex = 1;
            this.metroTileAjoutEtagere.Text = "Etagère";
            this.metroTileAjoutEtagere.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroTileAjoutEtagere.TileImage = ((System.Drawing.Image)(resources.GetObject("metroTileAjoutEtagere.TileImage")));
            this.metroTileAjoutEtagere.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.metroTileAjoutEtagere.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall;
            this.metroTileAjoutEtagere.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Bold;
            this.metroTileAjoutEtagere.UseTileImage = true;
            this.metroTileAjoutEtagere.Click += new System.EventHandler(this.metroTileAjoutEtagere_Click);
            // 
            // metroTileRemettreLivre
            // 
            this.metroTileRemettreLivre.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.metroTileRemettreLivre.CustomBackground = true;
            this.metroTileRemettreLivre.Location = new System.Drawing.Point(0, 318);
            this.metroTileRemettreLivre.Name = "metroTileRemettreLivre";
            this.metroTileRemettreLivre.Size = new System.Drawing.Size(196, 46);
            this.metroTileRemettreLivre.TabIndex = 2;
            this.metroTileRemettreLivre.Text = "Rendre";
            this.metroTileRemettreLivre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroTileRemettreLivre.TileImage = ((System.Drawing.Image)(resources.GetObject("metroTileRemettreLivre.TileImage")));
            this.metroTileRemettreLivre.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.metroTileRemettreLivre.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall;
            this.metroTileRemettreLivre.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Bold;
            this.metroTileRemettreLivre.UseTileImage = true;
            this.metroTileRemettreLivre.Click += new System.EventHandler(this.metroTileRemettreLivre_Click);
            // 
            // metroTileRetirerLivre
            // 
            this.metroTileRetirerLivre.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.metroTileRetirerLivre.CustomBackground = true;
            this.metroTileRetirerLivre.Location = new System.Drawing.Point(3, 243);
            this.metroTileRetirerLivre.Name = "metroTileRetirerLivre";
            this.metroTileRetirerLivre.Size = new System.Drawing.Size(193, 54);
            this.metroTileRetirerLivre.TabIndex = 3;
            this.metroTileRetirerLivre.Text = "Retirer";
            this.metroTileRetirerLivre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroTileRetirerLivre.TileImage = ((System.Drawing.Image)(resources.GetObject("metroTileRetirerLivre.TileImage")));
            this.metroTileRetirerLivre.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.metroTileRetirerLivre.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall;
            this.metroTileRetirerLivre.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Bold;
            this.metroTileRetirerLivre.UseTileImage = true;
            this.metroTileRetirerLivre.Click += new System.EventHandler(this.metroTileRetirerLivre_Click);
            // 
            // metroTileRemplacerLirve
            // 
            this.metroTileRemplacerLirve.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.metroTileRemplacerLirve.CustomBackground = true;
            this.metroTileRemplacerLirve.Location = new System.Drawing.Point(0, 384);
            this.metroTileRemplacerLirve.Name = "metroTileRemplacerLirve";
            this.metroTileRemplacerLirve.Size = new System.Drawing.Size(250, 56);
            this.metroTileRemplacerLirve.TabIndex = 4;
            this.metroTileRemplacerLirve.Text = "Remplacer";
            this.metroTileRemplacerLirve.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroTileRemplacerLirve.TileImage = ((System.Drawing.Image)(resources.GetObject("metroTileRemplacerLirve.TileImage")));
            this.metroTileRemplacerLirve.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.metroTileRemplacerLirve.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall;
            this.metroTileRemplacerLirve.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Bold;
            this.metroTileRemplacerLirve.UseTileImage = true;
            this.metroTileRemplacerLirve.Click += new System.EventHandler(this.metroTileRemplacerLirve_Click);
            // 
            // metroTileListeLivres
            // 
            this.metroTileListeLivres.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.metroTileListeLivres.CustomBackground = true;
            this.metroTileListeLivres.Location = new System.Drawing.Point(0, 459);
            this.metroTileListeLivres.Name = "metroTileListeLivres";
            this.metroTileListeLivres.Size = new System.Drawing.Size(169, 56);
            this.metroTileListeLivres.TabIndex = 5;
            this.metroTileListeLivres.Text = "Liste";
            this.metroTileListeLivres.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroTileListeLivres.TileImage = ((System.Drawing.Image)(resources.GetObject("metroTileListeLivres.TileImage")));
            this.metroTileListeLivres.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.metroTileListeLivres.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall;
            this.metroTileListeLivres.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Bold;
            this.metroTileListeLivres.UseTileImage = true;
            this.metroTileListeLivres.Click += new System.EventHandler(this.metroTileListeLivres_Click);
            // 
            // metroPanel1
            // 
            this.metroPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.metroPanel1.Controls.Add(this.metroTile2);
            this.metroPanel1.Controls.Add(this.metroTileAjoutEtagere);
            this.metroPanel1.Controls.Add(this.metroTileListeLivres);
            this.metroPanel1.Controls.Add(this.metroTileAjoutLivre);
            this.metroPanel1.Controls.Add(this.metroTileRemplacerLirve);
            this.metroPanel1.Controls.Add(this.metroTileRetirerLivre);
            this.metroPanel1.Controls.Add(this.metroTileRemettreLivre);
            this.metroPanel1.CustomBackground = true;
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(1, 5);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(218, 601);
            this.metroPanel1.TabIndex = 6;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // metroTile2
            // 
            this.metroTile2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.metroTile2.CustomBackground = true;
            this.metroTile2.CustomForeColor = true;
            this.metroTile2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.metroTile2.Location = new System.Drawing.Point(3, 530);
            this.metroTile2.Name = "metroTile2";
            this.metroTile2.Size = new System.Drawing.Size(196, 48);
            this.metroTile2.TabIndex = 6;
            this.metroTile2.Text = "Imprimer";
            this.metroTile2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroTile2.TileImage = ((System.Drawing.Image)(resources.GetObject("metroTile2.TileImage")));
            this.metroTile2.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.metroTile2.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall;
            this.metroTile2.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Bold;
            this.metroTile2.Click += new System.EventHandler(this.metroTile2_Click);
            // 
            // metroPanel2
            // 
            this.metroPanel2.BackColor = System.Drawing.Color.Brown;
            this.metroPanel2.CustomBackground = true;
            this.metroPanel2.HorizontalScrollbarBarColor = true;
            this.metroPanel2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel2.HorizontalScrollbarSize = 10;
            this.metroPanel2.Location = new System.Drawing.Point(218, 5);
            this.metroPanel2.Name = "metroPanel2";
            this.metroPanel2.Size = new System.Drawing.Size(868, 60);
            this.metroPanel2.TabIndex = 7;
            this.metroPanel2.VerticalScrollbarBarColor = true;
            this.metroPanel2.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel2.VerticalScrollbarSize = 10;
            // 
            // metroTile1
            // 
            this.metroTile1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.metroTile1.CustomBackground = true;
            this.metroTile1.Location = new System.Drawing.Point(983, 8);
            this.metroTile1.Name = "metroTile1";
            this.metroTile1.Size = new System.Drawing.Size(103, 51);
            this.metroTile1.TabIndex = 2;
            this.metroTile1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroTile1.TileImage = ((System.Drawing.Image)(resources.GetObject("metroTile1.TileImage")));
            this.metroTile1.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroTile1.UseTileImage = true;
            this.metroTile1.Click += new System.EventHandler(this.metroTile1_Click);
            // 
            // remplacer1
            // 
            this.remplacer1.BackColor = System.Drawing.Color.White;
            this.remplacer1.Location = new System.Drawing.Point(218, 65);
            this.remplacer1.Name = "remplacer1";
            this.remplacer1.Size = new System.Drawing.Size(868, 541);
            this.remplacer1.TabIndex = 13;
            this.remplacer1.Load += new System.EventHandler(this.remplacer1_Load);
            // 
            // rendre1
            // 
            this.rendre1.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.rendre1.Location = new System.Drawing.Point(218, 65);
            this.rendre1.Name = "rendre1";
            this.rendre1.Size = new System.Drawing.Size(868, 541);
            this.rendre1.TabIndex = 12;
            // 
            // retirerLivre1
            // 
            this.retirerLivre1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.retirerLivre1.Location = new System.Drawing.Point(218, 65);
            this.retirerLivre1.Name = "retirerLivre1";
            this.retirerLivre1.Size = new System.Drawing.Size(868, 541);
            this.retirerLivre1.TabIndex = 11;
            // 
            // listeLivres1
            // 
            this.listeLivres1.BackColor = System.Drawing.Color.LightBlue;
            this.listeLivres1.Location = new System.Drawing.Point(218, 65);
            this.listeLivres1.Name = "listeLivres1";
            this.listeLivres1.Size = new System.Drawing.Size(868, 541);
            this.listeLivres1.TabIndex = 10;
            // 
            // test11
            // 
            this.test11.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.test11.Location = new System.Drawing.Point(218, 65);
            this.test11.Name = "test11";
            this.test11.Size = new System.Drawing.Size(868, 541);
            this.test11.TabIndex = 9;
            this.test11.Load += new System.EventHandler(this.test11_Load);
            // 
            // ajoutLivre1
            // 
            this.ajoutLivre1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ajoutLivre1.Location = new System.Drawing.Point(218, 65);
            this.ajoutLivre1.Name = "ajoutLivre1";
            this.ajoutLivre1.Size = new System.Drawing.Size(868, 541);
            this.ajoutLivre1.TabIndex = 8;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1087, 606);
            this.Controls.Add(this.remplacer1);
            this.Controls.Add(this.rendre1);
            this.Controls.Add(this.retirerLivre1);
            this.Controls.Add(this.listeLivres1);
            this.Controls.Add(this.test11);
            this.Controls.Add(this.ajoutLivre1);
            this.Controls.Add(this.metroTile1);
            this.Controls.Add(this.metroPanel2);
            this.Controls.Add(this.metroPanel1);
            this.DisplayHeader = false;
            this.Name = "Form1";
            this.Padding = new System.Windows.Forms.Padding(20, 30, 20, 20);
            this.TextAlign = System.Windows.Forms.VisualStyles.HorizontalAlign.Center;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.metroPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroTile metroTileAjoutLivre;
        private MetroFramework.Controls.MetroTile metroTileAjoutEtagere;
        private MetroFramework.Controls.MetroTile metroTileRemettreLivre;
        private MetroFramework.Controls.MetroTile metroTileRetirerLivre;
        private MetroFramework.Controls.MetroTile metroTileRemplacerLirve;
        private MetroFramework.Controls.MetroTile metroTileListeLivres;
        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroPanel metroPanel2;
        private MetroFramework.Controls.MetroTile metroTile1;
        private AjoutLivre ajoutLivre1;
        private AjoutEtagere test11;
        private ListeLivres listeLivres1;
        private RetirerLivre retirerLivre1;
        private Rendre rendre1;
        private Remplacer remplacer1;
        private MetroFramework.Controls.MetroTile metroTile2;
    }
}

