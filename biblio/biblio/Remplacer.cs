﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace biblio
{
    public partial class Remplacer : UserControl
    {

        SqlConnection connection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\hp\Desktop\BARRY_Abdoulaye_Sadio\biblio\biblio\Biblio.mdf;Integrated Security=True");
        SqlDataAdapter dap;
        DataSet ds;

        public Remplacer()
        {
            InitializeComponent();
        }

        private void metroButton2_Click(object sender, EventArgs e)
        {
            this.SendToBack();
        }

        public void FillComboBox()
        {
            connection.Open();
            SqlCommand cmd = connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select numero from [etagere]";
            dap = new SqlDataAdapter(cmd);
            ds = new DataSet();
            dap.Fill(ds, "etagere");
            foreach (DataRow x in ds.Tables[0].Rows)
            {
                metroComboBox1.Items.Add(x[0].ToString());
            }
            connection.Close();
        }

        private void Remplacer_Load(object sender, EventArgs e)
        {
            FillComboBox();
        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            connection.Open();
            SqlCommand cmd = connection.CreateCommand();
            SqlCommand cmd2 = connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd2.CommandType = CommandType.Text;
            cmd.CommandText = "insert into [Livre] (code,titre,prix,etagere,disponible) values ('" + metroTextBoxCode.Text + "','" + metroTextBoxTitre.Text + "','" + float.Parse(metroTextBoxPrix.Text) + "','" + metroComboBox1.SelectedItem + "','oui')";
            cmd2.CommandText = "delete from [Livre] where code = '" + metroTextBox1.Text + "'";
            if (cmd2.ExecuteNonQuery() != 0)
            {
                cmd.ExecuteNonQuery();
                MessageBox.Show("Modification reussie!!!");
                metroTextBoxCode.Text = "";
                metroTextBoxTitre.Text = "";
                metroTextBoxPrix.Text = "";
            }
            connection.Close();
        }
    }
}
