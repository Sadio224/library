﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;

namespace biblio
{
    public partial class ListeLivres : UserControl
    {

        SqlConnection connection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\hp\Desktop\BARRY_Abdoulaye_Sadio\biblio\biblio\Biblio.mdf;Integrated Security=True");

        // Pour Crystal Report nous allons créer un DataTable et DataSet
        static DataSet dscompt = new DataSet("CommandeLivre");
        DataTable dtcompt = new DataTable("CommandeLivre");

        public ListeLivres()
        {
            InitializeComponent();
            display_data();
        }

        private void ListeLivres_Load(object sender, EventArgs e)
        {
            display_data();
        }

        private void display_data()
        {

            connection.Open();

            SqlCommand cmd = connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select * from [Livre] where disponible='oui'";

            cmd.ExecuteNonQuery();
            DataTable dta = new DataTable();
            SqlDataAdapter dataadp = new SqlDataAdapter(cmd.CommandText, connection);
            dataadp.Fill(dta);
            dataGridView1.DataSource = dta;

            connection.Close();
        }

        private void metroTile2_Click(object sender, EventArgs e)
        {
            display_data();
        }

        private void metroTextBox1_TextChanged(object sender, EventArgs e)
        {
            connection.Open();
            SqlCommand cmd = connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
          
            switch (metroComboBoxSearch.Text)
            {
                case "Titre" : cmd.CommandText = "select * from [Livre] where titre like'" + metroTextBox1.Text + "%'";
                    break;
                case "Code":
                    cmd.CommandText = "select * from [Livre] where code like'" + metroTextBox1.Text + "%'";
                    break;
                case "Prix":
                    cmd.CommandText = "select * from [Livre] where prix <='" + float.Parse(metroTextBox1.Text) + "'";
                    break;
                case "Etagere":
                    cmd.CommandText = "select * from [Livre] where etagere like'" + metroTextBox1.Text + "%'";
                    break;
                default:
                    cmd.CommandText = "";
                    break;
            }
            if(cmd.CommandText != "")
            {
                cmd.ExecuteNonQuery();
                DataTable dtab = new DataTable();
                SqlDataAdapter dadap = new SqlDataAdapter(cmd.CommandText, connection);
                dadap.Fill(dtab);
                dataGridView1.DataSource = dtab;
            }
            connection.Close();
        }

        string code = "";
        string titre = "";
        float prix;

        private void display_data2()
        {

            connection.Open();

            SqlCommand cmd = connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            if (code.Length == 0)
            {
                this.Controls.Add(new AjoutLivre());
                this.SendToBack();
            }
            else
            {
                cmd.CommandText = "select * from [Commande] ";

                cmd.ExecuteNonQuery();
                DataTable dta = new DataTable();
                SqlDataAdapter dataadp = new SqlDataAdapter(cmd.CommandText, connection);
                dataadp.Fill(dta);
                dataGridView3.DataSource = dta;
            }

            connection.Close();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void metroTextBox1_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int SelectedRow = 0;
            SelectedRow = e.RowIndex;
            if (SelectedRow != -1)
            {

                DataGridViewRow row = dataGridView1.Rows[SelectedRow];
                code = row.Cells["codeDataGridViewTextBoxColumn"].Value.ToString();
                titre = row.Cells["titreDataGridViewTextBoxColumn"].Value.ToString();
                prix = float.Parse(row.Cells["prixDataGridViewTextBoxColumn"].Value.ToString());
                /*metroTextBNom.Text = row.Cells["Nom"].Value.ToString();
                metroTextBVille.Text = row.Cells["Ville"].Value.ToString();
                metroTextBNiveau.Text = row.Cells["Niveau"].Value.ToString();
                metroTextBCNE.Text = row.Cells["CNE"].Value.ToString();*/
            }
        }

        private void dataGridView3_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        int numRow;
        private void dataGridView3_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            numRow = e.RowIndex;
        }

        private void metroTile1_Click(object sender, EventArgs e)
        {
            connection.Open();
            SqlCommand cmd = connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "insert into Commande(code,titre,prix) values ('" + code + "','" + titre + "','" + prix + "')";
            cmd.ExecuteNonQuery();
            connection.Close();

            metroTextBoxTotal.Text = (float.Parse(metroTextBoxTotal.Text) + prix).ToString();
            display_data2();
        }

        private void metroTile3_Click(object sender, EventArgs e)
        {
            float prix2;
            string code;
            DataGridViewRow row = dataGridView3.Rows[numRow];
            prix2 = float.Parse(row.Cells["prixDataGridViewTextBoxColumn1"].Value.ToString());
            code = row.Cells["codeDataGridViewTextBoxColumn1"].Value.ToString();
            dataGridView3.Rows.RemoveAt(numRow);
            metroTextBoxTotal.Text = (float.Parse(metroTextBoxTotal.Text) - prix2).ToString();

            connection.Open();
            SqlCommand cmd = connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "delete from [Commande] where code = '" + code + "'";
            cmd.ExecuteNonQuery();
            connection.Close();
        }

        private void metroTile4_Click(object sender, EventArgs e)
        {
            //Pour remplir dscompt
            if (dataGridView3.DataSource != null)
            {


                dtcompt.Clear();
                dtcompt.Columns.Clear();

                //Appel methode création Dscompt Dataset (voir sa définition)
                createdscompt();

                // Appel de l’objet Print_Facture() qui représente crystalReportViewer
                Form2 obj = new Form2();

                obj.Show();

                // Ceci permet de vider dscompt des données remplies par la méthode createdscompt()
                // et pour pouvoir la réutiliser afin d'imprimer une nouvelle table ["FactureCompt"] 
                // apres selection 

                dscompt.Tables.Remove(dscompt.Tables["CommandeLivre"]);

            }
            else
            {
                MessageBox.Show("Aucune Commande à Imprimer");
            }
        }
        
        private void createdscompt()
        {
            if (dtcompt.Rows.Count <= 0)
            {

                DataColumn dc1 = new DataColumn("code", typeof(string));
                DataColumn dc2 = new DataColumn("titre", typeof(string));
                DataColumn dc3 = new DataColumn("prix", typeof(float));

                dtcompt.Columns.Add(dc1);
                dtcompt.Columns.Add(dc2);
                dtcompt.Columns.Add(dc3); 


                foreach (DataGridViewRow dgr in dataGridView3.Rows)
                {
                    dtcompt.Rows.Add(dgr.Cells["codeDataGridViewTextBoxColumn1"].Value, dgr.Cells["titreDataGridViewTextBoxColumn1"].Value, dgr.Cells["prixDataGridViewTextBoxColumn1"].Value);

                }

                // Remplissage de dataset dscompt
                dscompt.Tables.Add(dtcompt);

            }
            else
            {

                foreach (DataGridViewRow dgr in dataGridView3.Rows)
                {
                    dtcompt.Rows.Add(dgr.Cells["codeDataGridViewTextBoxColumn1"].Value, dgr.Cells["titreDataGridViewTextBoxColumn1"].Value, dgr.Cells["prixDataGridViewTextBoxColumn1"].Value);

                }
                dscompt.Tables.Add(dtcompt);
            }

        }
        // Pour Crystal Report
        public DataSet returndata()
        {
            return dscompt;

        }
    }
}
