﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;

namespace biblio
{
    public partial class AjoutLivre : UserControl
    {
        SqlConnection connection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\hp\Desktop\BARRY_Abdoulaye_Sadio\biblio\biblio\Biblio.mdf;Integrated Security=True");
        SqlDataAdapter dap;
        DataSet ds;

        public AjoutLivre()
        {
            InitializeComponent();
            
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void metroTile1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void metroTile2_Click(object sender, EventArgs e)
        {
            connection.Open();
            SqlCommand cmd = connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "insert into [Livre] (code,titre,prix,etagere,disponible) values ('" + metroTextBoxCode.Text + "','" + metroTextBoxTitre.Text + "','" + float.Parse(metroTextBoxPrix.Text) + "','" + metroComboBox1.SelectedItem + "','oui')";
            cmd.ExecuteNonQuery();
            connection.Close();
            MessageBox.Show("Livre enregistré!!!");
            metroTextBoxCode.Text = "";
            metroTextBoxTitre.Text = "";
            metroTextBoxPrix.Text = "";
        }

        private void AjoutLivre_Load(object sender, EventArgs e)
        {
            FillComboBox();
        }

        public void FillComboBox()
        {
            connection.Open();
            SqlCommand cmd = connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select numero from [etagere]";
            dap = new SqlDataAdapter(cmd);
            ds = new DataSet();
            dap.Fill(ds, "etagere");
            foreach (DataRow x in ds.Tables[0].Rows)
            {
                metroComboBox1.Items.Add(x[0].ToString());
            }
            connection.Close();
        }

        private void metroComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }
    }
}
